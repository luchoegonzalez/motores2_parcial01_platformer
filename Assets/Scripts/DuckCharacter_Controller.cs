using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckCharacter_Controller : Controller_Player
{
    int i = 0;
    private float initialSize;
    public float upDistanceRay;

    public override void Start()
    {
        base.Start();
        initialSize = rb.transform.localScale.y;
    }

    public override void Update()
    {
        Agacharse();
        base.Update();
    }

    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position + new Vector3(0, 0.5f, 0), new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay) && !downHit.collider.isTrigger && !downHit.collider.gameObject.CompareTag("ColorWall");
    }

    public override bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y + (transform.localScale.y / 9f), transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out rightHit, distanceRay) && !rightHit.collider.isTrigger && !rightHit.collider.gameObject.CompareTag("ColorWall") && !rightHit.collider.gameObject.CompareTag("ColorButton");
    }

    public override bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y + (transform.localScale.y / 9f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
        return Physics.Raycast(landingRay, out leftHit, distanceRay) && !leftHit.collider.isTrigger && !leftHit.collider.gameObject.CompareTag("ColorWall") && !leftHit.collider.gameObject.CompareTag("ColorButton");
    }

    internal void Agacharse()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Transform flecha = transform.GetChild(0);
            if (IsOnSomething())
            {
                if (Input.GetKey(KeyCode.S))
                {
                    if (i == 0)
                    {
                        rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                        flecha.localScale = new Vector3(flecha.localScale.x, flecha.localScale.y * 2, flecha.localScale.z);
                        i++;
                    }
                }
                else
                {
                    if (rb.transform.localScale.y != initialSize)
                    {
                        ActualizarPersonajeDeArriba();
                        rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                        flecha.localScale = new Vector3(flecha.localScale.x, flecha.localScale.y / 2, flecha.localScale.z);
                        i = 0;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.S))
                {
                    rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
                }
            }
        } else
        {
            ActualizarPersonajeDeArriba();
            rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
            i = 0;
        }
    }

    void ActualizarPersonajeDeArriba()
    {
        RaycastHit hit;
        Vector3 rayOrigin = transform.position + Vector3.up * 0.5f;
        if (Physics.BoxCast(rayOrigin, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.up, out hit, Quaternion.identity, upDistanceRay))
        {
            if (hit.collider.CompareTag("Player"))
            {
                hit.collider.transform.position += new Vector3(0, initialSize / 2, 0);
            }
        }
        Physics.SyncTransforms();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float jumpForce = 10;

    public float speed = 5;

    public int playerNumber;

    public Rigidbody rb;

    internal BoxCollider col;

    public LayerMask floor;

    internal RaycastHit leftHit,rightHit,downHit;

    public float distanceRay,downDistanceRay;

    private bool canMoveLeft, canMoveRight,canJump;
    internal bool onFloor;

    internal AudioSource audioSource;


    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        audioSource = GameObject.Find("SoundContainer").GetComponent<AudioSource>();
        rb.constraints = RigidbodyConstraints.FreezePositionX| RigidbodyConstraints.FreezePositionZ|RigidbodyConstraints.FreezeRotation;
    }

    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();
        }
    }

    public virtual void Update()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Jump();
            if (SomethingLeft())
            {
                canMoveLeft = false;
            }
            else
            {
                canMoveLeft = true;
            }
            if (SomethingRight())
            {
                canMoveRight = false;
            }
            else
            {
                canMoveRight = true;
            }

            if (IsOnSomething())
            {
                canJump = true;
            }
            else
            {
                canJump = false;
            }

        }
        else
        {
            if (onFloor)
            {
                rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                if (IsOnSomething())
                {
                    if (downHit.collider.gameObject.CompareTag("Player") || downHit.collider.gameObject.CompareTag("Plataforma"))
                    {
                        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    }
                }
            }
        }
    }




    public virtual bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.6f, transform.localScale.y/3,transform.localScale.z*0.6f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay) && !downHit.collider.isTrigger && !downHit.collider.gameObject.CompareTag("ColorWall");
    }

    public virtual bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x,transform.position.y-(transform.localScale.y / 2.1f),transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);

        //comparo si el hit toca es un trigger o una pared de color para que no se frene en donde no deberia colisionar
        return Physics.Raycast(landingRay, out rightHit, distanceRay) && !rightHit.collider.isTrigger && !rightHit.collider.gameObject.CompareTag("ColorWall") && !rightHit.collider.gameObject.CompareTag("ColorButton");
    }

    public virtual bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y/2.1f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);

        //comparo si el hit toca es un trigger o una pared de color para que no se frene en donde no deberia colisionar
        return Physics.Raycast(landingRay, out leftHit, distanceRay) && !leftHit.collider.isTrigger && !leftHit.collider.gameObject.CompareTag("ColorWall") && !leftHit.collider.gameObject.CompareTag("ColorButton");
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.A) && canMoveLeft)
        {
                rb.velocity = new Vector3(1 * -speed , rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.D) && canMoveRight)
        {
                rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
        //if (!canMoveLeft)
        //    rb.velocity = new Vector3(0, rb.velocity.y, 0);
        //if (!canMoveRight)
        //    rb.velocity = new Vector3(0, rb.velocity.y, 0);
    }

    public virtual void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (canJump)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                audioSource.PlayOneShot(GameManager.instancia.saltoSonido);
            }
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water") || collision.gameObject.CompareTag("Enemigo") || collision.gameObject.CompareTag("ColorWall"))
        {
            Destroy(this.gameObject);
            audioSource.PlayOneShot(GameManager.instancia.gameOverSonido);
            GameManager.gameOver = true;
        }
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Estrella"))
        {
            GameManager.instancia.puntos++;
            audioSource.PlayOneShot(GameManager.instancia.estrellaSonido);
            Destroy(other.gameObject);
        }
    }
}
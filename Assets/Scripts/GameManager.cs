﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool gameOver = false;

    public static bool winCondition = false;

    public static int actualPlayer = 0;

    public List<Controller_Target> targets;

    public List<GameObject> players;

    public event Action CambioDePersonaje;
    public static GameManager instancia;

    public int puntos;

    public AudioClip saltoSonido;
    public AudioClip estrellaSonido;
    public AudioClip gameOverSonido;
    public AudioClip winSonido;

    private void Awake()
    {
        if (instancia != null && instancia != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instancia = this;
        }
    }

    void Start()
    {
        ReiniciarGameManager();
    }

    public void ReiniciarGameManager()
    {
        puntos = 0;
        actualPlayer = 0;
        Physics.gravity = new Vector3(0, -30, 0);
        gameOver = false;
        winCondition = false;
        SetConstraits();
        CambiarDePersonaje();
    }

    void Update()
    {
        GetInput();
        CheckWin();

    }

    private void CheckWin()
    {
        int i = 0;
        foreach (Controller_Target t in targets)
        {
            if (t.playerOnTarget)
            {
                i++;
                //Debug.Log(i.ToString());
            }
        }
        if (i >= 4 && !winCondition)
        {
            winCondition = true;
            GameObject.Find("SoundContainer").GetComponent<AudioSource>().PlayOneShot(winSonido, 1f);
        }
    }

    private void GetInput()
    {
        if (!winCondition && !gameOver)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (actualPlayer <= 0)
                {
                    actualPlayer = players.Count - 1;
                    SetConstraits();
                    CambiarDePersonaje();
                }
                else
                {
                    actualPlayer--;
                    SetConstraits();
                    CambiarDePersonaje();
                }
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (actualPlayer >= players.Count - 1)
                {
                    actualPlayer = 0;
                    SetConstraits();
                    CambiarDePersonaje();
                }
                else
                {
                    actualPlayer++;
                    SetConstraits();
                    CambiarDePersonaje();
                }
            }
        }
    }

    public void CambiarDePersonaje()
    {
        CambioDePersonaje?.Invoke();
    }

    private void SetConstraits()
    {
        foreach(GameObject p in players)
        {
            Controller_Player pController = p.GetComponent<Controller_Player>();
            if (p == players[actualPlayer])
            {
                pController.rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                pController.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuedaDeColores_Controller : MonoBehaviour
{
    public float rapidezRotacion;
    void Update()
    {
        transform.Rotate(new Vector3(0,0,rapidezRotacion * Time.deltaTime));
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestorDeColores : MonoBehaviour
{
    public static Material colorAzul, colorRojo, colorVerde, colorAmarillo;

    public Material materialAzul, materialRojo, materialVerde, materialAmarillo;

    private void Awake()
    {
        ValidarColores();
    }

    private void OnValidate()
    {
        ValidarColores();
    }

    private void ValidarColores()
    {
        colorAzul = materialAzul;
        colorRojo = materialRojo;
        colorVerde = materialVerde;
        colorAmarillo = materialAmarillo;
    }
}

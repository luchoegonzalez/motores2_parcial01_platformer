using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow_Controller : MonoBehaviour
{
    private List<GameObject> players;

    private void Awake()
    {
        GameManager.instancia.CambioDePersonaje += CambiarPosicionFlecha;
        players = GameManager.instancia.players;
    }

    private void Update()
    {
        ActualizarColor();
    }

    public void CambiarPosicionFlecha ()
    {
        if (players[GameManager.actualPlayer] != null)
        {
            float parteArribaJugador = players[GameManager.actualPlayer].GetComponent<BoxCollider>().bounds.max.y;
            Vector3 posicionJugador = players[GameManager.actualPlayer].transform.position;
            transform.position = new Vector3(posicionJugador.x, parteArribaJugador + 0.6f, -1.5f);
            transform.SetParent(players[GameManager.actualPlayer].transform);
        }
    }

    public void ActualizarColor()
    {
        transform.GetChild(0).GetComponent<MeshRenderer>().material.color = players[GameManager.actualPlayer].GetComponent<MeshRenderer>().material.color;
    }

}

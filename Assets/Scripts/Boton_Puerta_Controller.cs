using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton_Puerta_Controller : ColoredItem
{
    internal GameObject puerta;
    public float rapidezElevacion;
    private bool botonActivado = false;
    private AudioSource sonido;

    internal override void Start()
    {
        puerta = transform.parent.GetChild(0).gameObject;
        sonido = GetComponent<AudioSource>();
        if (color != puerta.GetComponent<ColoredItem>().color)
        {
            Debug.Log("La puerta y el boton no tienen el mismo color!!!! - " + transform.parent.name);
        }
        base.Start();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && color == puerta.GetComponent<ColoredItem>().color)
        {
            Controller_Player scriptJugador = collision.gameObject.GetComponent<Controller_Player>();
            if ((color == OpcionesDeColores.rojo && scriptJugador.playerNumber == 0) || (color == OpcionesDeColores.azul && scriptJugador.playerNumber == 1) || (color == OpcionesDeColores.verde && scriptJugador.playerNumber == 2) || (color == OpcionesDeColores.amarillo && scriptJugador.playerNumber == 3))
            {
                if (!botonActivado)
                {
                    sonido.Play();
                    botonActivado = true;
                }
            }
        }
    }

    internal void Update()
    {
        if (botonActivado && puerta.transform.localPosition.y <= 12)
        {
            ElevarPuerta();
        }
    }

    internal void ElevarPuerta ()
    {
        puerta.transform.localPosition += new Vector3(0, rapidezElevacion, 0) * Time.deltaTime;
    }
}

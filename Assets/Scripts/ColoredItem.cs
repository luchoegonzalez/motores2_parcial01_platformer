using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ColoredItem : MonoBehaviour
{
    public enum OpcionesDeColores
    {
        rojo,
        azul,
        verde,
        amarillo
    }
    public OpcionesDeColores color;

    internal MeshRenderer meshPared;
    internal virtual void Start()
    {
        SincronizarColores();
    }

    internal virtual void OnValidate()
    {
        meshPared = GetComponent<MeshRenderer>();
        if (GestorDeColores.colorRojo != null && GestorDeColores.colorAmarillo != null && GestorDeColores.colorAzul != null && GestorDeColores.colorVerde != null)
        {
            SincronizarColores();
        }
    }

    private void SincronizarColores()
    {
        if (color == OpcionesDeColores.rojo)
        {
            meshPared.material = GestorDeColores.colorRojo;
        }
        else if (color == OpcionesDeColores.azul)
        {
            meshPared.material = GestorDeColores.colorAzul;
        }
        else if (color == OpcionesDeColores.verde)
        {
            meshPared.material = GestorDeColores.colorVerde;
        }
        else if (color == OpcionesDeColores.amarillo)
        {
            meshPared.material = GestorDeColores.colorAmarillo;
        }
    }

    internal virtual void OnInspectorGUI()
    {
        color = (OpcionesDeColores)EditorGUILayout.EnumPopup("Dropdown Options", color);
    }
}

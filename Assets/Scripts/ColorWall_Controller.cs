using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ColorWall_Controller : ColoredItem
{
    internal override void Start()
    {
        if (color == OpcionesDeColores.rojo)
        {
            Physics.IgnoreCollision(GameManager.instancia.players[0].GetComponent<Collider>(), GetComponent<Collider>());
        } else if (color == OpcionesDeColores.azul)
        {
            Physics.IgnoreCollision(GameManager.instancia.players[1].GetComponent<Collider>(), GetComponent<Collider>());
        } else if (color == OpcionesDeColores.verde)
        {
            Physics.IgnoreCollision(GameManager.instancia.players[2].GetComponent<Collider>(), GetComponent<Collider>());
        }
        else if (color == OpcionesDeColores.amarillo)
        {
            Physics.IgnoreCollision(GameManager.instancia.players[3].GetComponent<Collider>(), GetComponent<Collider>());
        }
        base.Start();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeteccionDePersonaje : MonoBehaviour
{
    private Boton_Plataforma_Controller boton;

    private void Start()
    {
        boton = transform.parent.parent.GetChild(1).gameObject.GetComponent<Boton_Plataforma_Controller>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            boton.personajeDebajo = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            boton.personajeDebajo = false;
        }
    }
}

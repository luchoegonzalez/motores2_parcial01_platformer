﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Floating : Controller_Player
{
    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.48f, transform.localScale.y / 3, transform.localScale.z * 0.48f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay) && !downHit.collider.isTrigger && !downHit.collider.gameObject.CompareTag("ColorWall");
    }
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }

        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Destroy(this.gameObject);
            audioSource.PlayOneShot(GameManager.instancia.gameOverSonido);
            GameManager.gameOver = true;
        }
        //This makes the player invulnerable to water.
    }
}

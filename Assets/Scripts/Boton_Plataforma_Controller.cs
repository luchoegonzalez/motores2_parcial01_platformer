using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton_Plataforma_Controller : MonoBehaviour
{
    public Vector3 posicion1;
    public Vector3 posicion2;
    public float rapidez = 1.0f;

    private Rigidbody rbPlataforma;
    private Vector3 targetPosition;
    private bool moviendoAPosicion1 = true;
    public bool personajeDebajo = false;
    bool botonActivado = false;
    private AudioSource sonido;

    void Start()
    {
        rbPlataforma = transform.parent.GetChild(0).gameObject.GetComponent<Rigidbody>();
        targetPosition = posicion1;
        sonido = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (!personajeDebajo && moviendoAPosicion1 && botonActivado)
        {
            MoverPlataforma();
        }

        if (!moviendoAPosicion1 && botonActivado)
        {
            MoverPlataforma();
        }
    }

    void MoverPlataforma()
    {
        if (rbPlataforma == null) return;

        Vector3 direccion = (targetPosition - rbPlataforma.position).normalized;
        Vector3 nuevaPosicion = rbPlataforma.position + direccion * rapidez * Time.deltaTime;

        rbPlataforma.MovePosition(nuevaPosicion);

        if (Vector3.Distance(rbPlataforma.position, targetPosition) < 0.1f)
        {
            if (moviendoAPosicion1)
            {
                targetPosition = posicion2;
            }
            else
            {
                targetPosition = posicion1;
            }
            moviendoAPosicion1 = !moviendoAPosicion1;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!botonActivado)
            {
                sonido.Play();
                botonActivado = true;
            }
        }
    }
}

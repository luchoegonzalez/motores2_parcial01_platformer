﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;
    public Text starsText;

    AudioSource musica;

    void Start()
    {
        gameOverText.gameObject.SetActive(false);
        musica = GameObject.Find("MusicContainer").GetComponent<AudioSource>();
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over" ;
            gameOverText.gameObject.SetActive(true);
            musica.Stop();
        }
        if (GameManager.winCondition)
        {
            Time.timeScale = 0;
            gameOverText.text = "You Win";
            gameOverText.gameObject.SetActive(true);
            musica.Stop();
        }

        starsText.text = "Stars: " + GameManager.instancia.puntos;
    }
}

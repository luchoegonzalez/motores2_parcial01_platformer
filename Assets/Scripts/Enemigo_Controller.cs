using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Controller : MonoBehaviour
{
    public float rapidez = 1.0f;

    public Vector3[] posiciones;
    private int posicionActual = 0;

    void Start()
    {
        transform.position = posiciones[posicionActual];
    }

    void Update()
    {
        MoverASigPosicion();
    }

    void MoverASigPosicion()
    {
        transform.position = Vector3.MoveTowards(transform.position, posiciones[posicionActual], rapidez * Time.deltaTime);

        if (Vector3.Distance(transform.position, posiciones[posicionActual]) < 0.1f)
        {
            posicionActual = (posicionActual + 1) % posiciones.Length;
        }
    }
}

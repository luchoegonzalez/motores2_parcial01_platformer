# Motores Gráficos II - Parcial 01 - Platformer

Juego de plataformas realizado como 1er parcial de la materia Motores Gráficos 2.
Programación, arte y música realizados por mi.

### Consigna
Sobre el proyecto "Platformer" se deberán aplicar las siguientes consignas:

- 1 Mecánicas coherente con el diseño
- 1 Estética definida y coherente con el diseño
- 5 Assets con comportamiento único
- Uno de dichos assets debe ser un nuevo personaje
- 3 Shaders
- Descubrir y arreglar los bugs del proyecto
- Mejorar la experiencia de Gameplay agregando un nivel


### Video
[![Watch the video](https://img.youtube.com/vi/hqd6Sj_bIdA/hqdefault.jpg)](https://youtu.be/hqd6Sj_bIdA)